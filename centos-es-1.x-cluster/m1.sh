#!/bin/bash

chmod +x /vagrant/common.sh
/vagrant/common.sh

cp /etc/elasticsearch/elasticsearch.yml /etc/elasticsearch/elasticsearch.yml.bak
cp /vagrant/elasticsearch.m1.yml /etc/elasticsearch/elasticsearch.yml

#install and run nginx with default setting
cp /vagrant/nginx.repo /etc/yum.repos.d/nginx.repo
yum search nginx
yum install -y nginx
/sbin/chkconfig --add nginx
service nginx start
 
#install kibana in home directory
KIBANA_VERSION="3.0.1"
wget https://download.elasticsearch.org/kibana/kibana/kibana-$KIBANA_VERSION.tar.gz
tar -xzf kibana-$KIBANA_VERSION.tar.gz
