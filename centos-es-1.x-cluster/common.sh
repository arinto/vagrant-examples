#!/bin/bash

#install additional utilities here
yum install -y vim mlocate
updatedb 

wget http://stedolan.github.io/jq/download/linux64/jq -P bin
chmod +x bin/jq

#install Oracle JRE
JRE_RPM="/vagrant/jre-7u51-linux-x64.rpm"
if [ ! -f $JRE_RPM ]; then
  wget -O $JRE_RPM --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/7u51-b13/jre-7u51-linux-x64.rpm" 
fi

rpm -Uvh $JRE_RPM
alternatives --install /usr/bin/java java /usr/java/jre1.7.0_51/bin/java 200000
alternatives --install /usr/bin/javaws javaws /usr/java/jre1.7.0_51/bin/javaws 200000
#alternatives --install /usr/lib/mozilla/plugins/libjavaplugin.so libjavaplugin.so /usr/java/jre1.7.0_51/lib/i386/libnpjp2.so 200000
#alternatives --install /usr/lib64/mozilla/plugins/libjavaplugin.so libjavaplugin.so.x86_64 /usr/java/jre1.7.0_51/lib/amd64/libnpjp2.so 200000

#set the OS limit
LIMIT_FILE="/etc/security/limits.conf"
echo "" >> $LIMIT_FILE
echo "elasticsearch soft nofile 64000" >> $LIMIT_FILE
echo "elasticsearch hard nofile 64000" >> $LIMIT_FILE

ulimit -l unlimited

#install and run elasticsearch with default setting, install head and paramedic plugin
ES_VERSION="1.1.0"
ES_RPM="/vagrant/elasticsearch-$ES_VERSION.noarch.rpm"
if [ ! -f $ES_RPM ]; then
  wget -O $ES_RPM https://download.elasticsearch.org/elasticsearch/elasticsearch/elasticsearch-$ES_VERSION.noarch.rpm
fi
rpm -Uvh $ES_RPM
chsh -s /bin/bash elasticsearch
/sbin/chkconfig --add elasticsearch

/usr/share/elasticsearch/bin/plugin --install mobz/elasticsearch-head
/usr/share/elasticsearch/bin/plugin --install karmi/elasticsearch-paramedic

#install logstash
LOGSTASH_VERSION="1.4.0-1_c82dc09"
LOGSTASH_RPM="/vagrant/logstash-$LOGSTASH_VERSION.noarch.rpm"
if [ ! -f $LOGSTASH_RPM ]; then
  wget -O $LOGSTASH_RPM https://download.elasticsearch.org/logstash/logstash/packages/centos/logstash-$LOGSTASH_VERSION.noarch.rpm
fi
rpm -Uvh $LOGSTASH_RPM
