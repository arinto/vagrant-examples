#!/bin/bash

FIRST_TIME="/home/vagrant/zk-first.file"
if [ -f $FIRST_TIME ];then
  echo "$FIRST_TIME exists, stop provisioning"
  exit
fi

#Install ZooKeeper
echo "Installing Zookeeper.."
ZK="zookeeper-3.4.6"
ZK_TAR_GZ="/vagrant/$ZK.tar.gz"
ZK_MIRROR="http://mirror.nus.edu.sg/apache/zookeeper/stable/zookeeper-3.4.6.tar.gz"
if [ ! -f $ZK_TAR_GZ ];then
  wget --quiet "$ZK_MIRROR" -O $ZK_TAR_GZ 
fi

tar xzf $ZK_TAR_GZ -C /opt/
ln -s /opt/$ZK /opt/zk
mkdir /home/vagrant/zk
mkdir /home/vagrant/zk/dataDir
chown -R vagrant:vagrant /home/vagrant/zk
cp /vagrant/zoo.cfg /opt/zk/conf/zoo.cfg
echo "Finish installing Zookeeper.."

#Set environment variable for ZK
echo "ZK_HOME=/opt/zk" >> /home/vagrant/.bash_profile
echo "PATH=\$ZK_HOME/bin:\$PATH" >> /home/vagrant/.bash_profile
echo "export PATH" >> /home/vagrant/.bash_profile

touch $FIRST_TIME
