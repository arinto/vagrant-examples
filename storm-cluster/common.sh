#!/bin/bash

FIRST_TIME="/home/vagrant/first.file"
if [ -f $FIRST_TIME ];then
  echo "$FIRST_TIME exists, stop provisioning"
  exit
fi
#install additional utilities here
echo "Installing additional utilities.."
yum install -y vim mlocate man nc telnet gcc gcc-c++ libuuid-devel git libtool unzip
updatedb 
echo "Finish installing additional utilities.."

#install Oracle JDK
echo "Installing Oracle JDK.."
JDK_RPM="/vagrant/jdk-7u60-linux-x64.rpm"
if [ ! -f $JDK_RPM ]; then
  wget --quiet --no-cookies --no-check-certificate --header "Cookie: oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/7u60-b19/jdk-7u60-linux-x64.rpm" -O $JDK_RPM 
fi

rpm -Uvh $JDK_RPM
alternatives --install /usr/bin/java java /usr/java/jdk1.7.0_60/bin/java 200000
alternatives --install /usr/bin/javaws javaws /usr/java/jdk1.7.0_60/bin/javaws 200000
#alternatives --install /usr/lib/mozilla/plugins/libjavaplugin.so libjavaplugin.so /usr/java/jre1.7.0_51/lib/i386/libnpjp2.so 200000
#alternatives --install /usr/lib64/mozilla/plugins/libjavaplugin.so libjavaplugin.so.x86_64 /usr/java/jre1.7.0_51/lib/amd64/libnpjp2.so 200000
echo "Finish installing Oracle JDK.."

#set environment variable
echo "Setting environment variable.."
echo "JAVA_HOME=/usr/java/latest" >> /home/vagrant/.bash_profile
echo "export JAVA_HOME" >> /home/vagrant/.bash_profile
echo "PATH=\$JAVA_HOME/bin:\$PATH" >> /home/vagrant/.bash_profile
echo "export PATH" >> /home/vagrant/.bash_profile
echo "Finish setting environment variable.."

#installing ZeroMQ
echo "Installing ZeroMQ..."
ZMQ="/vagrant/zeromq-2.1.7.tar.gz"
if [ ! -f $ZMQ ];then
  wget --quiet http://download.zeromq.org/zeromq-2.1.7.tar.gz -O $ZMQ
fi
tar -xzf $ZMQ -C /tmp/
chown -R vagrant:vagrant /tmp/zeromq-2.1.7
cd /tmp/zeromq-2.1.7
su vagrant -c 'echo "Using user `whoami`"'
su vagrant -c './configure'
su vagrant -c 'make'
echo "User user `whoami`"
make install
cd

if [ -d jzmq ];then
  rm -rf jzmq
fi
echo "Finish installing ZeroMQ..."

#installing jzmq
echo "Installing JZMQ.."
git clone https://github.com/nathanmarz/jzmq.git /home/vagrant/jzmq
chown -R vagrant:vagrant /home/vagrant/jzmq
cd /home/vagrant/jzmq
su vagrant -c 'env JAVA_HOME=/usr/java/latest ./autogen.sh'
su vagrant -c 'env JAVA_HOME=/usr/java/latest ./configure --prefix=/usr'
make install
cd
echo "Finish installing JZMQ.."

#create dedicated Storm user
echo "Creating dedicated Storm user.."
groupadd -g 53001 storm
mkdir -p /app/home
useradd -u 53001 -g 53001 -d /app/home/storm -s /bin/bash storm -c "Storm service account"
chmod 700 /app/home/storm
chage -I -1 -E -1 -m -1 -M -1 -W -1 -E -1 storm
echo "Finish creating dedicated Storm user.."

echo "Installing Storm.."
STORM_VERSION="storm-0.8.2"
STORM_ZIP="/vagrant/storm-0.8.2.zip"
STORM_URL="https://www.dropbox.com/s/fl4kr7w0oc8ihdw/storm-0.8.2.zip"
if [ ! -f $STORM_ZIP ];then
  wget --quiet $STORM_URL -O $STORM_ZIP
fi
cd /usr/local
unzip $STORM_ZIP 
chown -R storm:storm $STORM_VERSION
ln -s $STORM_VERSION storm

mkdir -p /app/storm
chown -R storm:storm /app/storm
chmod 750 /app/storm
echo "Finish installing Storm.."

#Install ZooKeeper
#echo "Installing Zookeeper.."
#ZK="zookeeper-3.4.6"
#ZK_TAR_GZ="/vagrant/$ZK.tar.gz"
#ZK_MIRROR="http://mirror.nus.edu.sg/apache/zookeeper/stable/zookeeper-3.4.6.tar.gz"
#if [ ! -f $ZK_TAR_GZ ];then
#  wget --quiet "$ZK_MIRROR" -O $ZK_TAR_GZ 
#fi
#
#tar xzf $ZK_TAR_GZ -C /opt/
#ln -s /opt/$ZK /opt/zk
#mkdir /home/vagrant/zk
#mkdir /home/vagrant/zk/dataDir
#chown -R vagrant:vagrant /home/vagrant/zk
#cp /vagrant/zoo.cfg /opt/zk/conf/zoo.cfg
#echo "Finish installing Zookeeper.."
#
##Set environment variable for ZK
#echo "ZK_HOME=/opt/zk" >> /home/vagrant/.bash_profile
#echo "PATH=\$ZK_HOME/bin:\$PATH" >> /home/vagrant/.bash_profile
#echo "export PATH" >> /home/vagrant/.bash_profile

touch $FIRST_TIME
