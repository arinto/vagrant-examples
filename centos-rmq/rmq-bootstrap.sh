#!/bin/bash

wget http://packages.erlang-solutions.com/erlang-solutions-1.0-1.noarch.rpm
rpm -Uvh erlang-solutions-1.0-1.noarch.rpm
yum --enablerepo=erlang-solutions -y install erlang
rpm --import http://www.rabbitmq.com/rabbitmq-signing-key-public.asc
wget http://www.rabbitmq.com/releases/rabbitmq-server/v3.2.3/rabbitmq-server-3.2.3-1.noarch.rpm
yum install -y rabbitmq-server-3.2.3-1.noarch.rpm
/sbin/chkconfig --add rabbitmq-server
rabbitmq-plugins enable rabbitmq_management
