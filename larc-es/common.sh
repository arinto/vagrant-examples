#!/bin/bash

#install additional utilities here
yum install -y vim mlocate
updatedb 

if [ ! -f bin/jq ];then 
  wget http://stedolan.github.io/jq/download/linux64/jq -P bin
  chmod +x bin/jq
  chown vagrant:vagrant bin/jq
fi

#install Oracle JRE
JRE_VERSION="8u51"
JRE_BUILD=16
JRE_RPM="/vagrant/jre-$JRE_VERSION-linux-x64.rpm"
if [ ! -f $JRE_RPM ]; then
  wget --no-cookies --no-check-certificate --header "Cookie: oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/$JRE_VERSION-b$JRE_BUILD/jre-$JRE_VERSION-linux-x64.rpm" -O $JRE_RPM 
fi

JRE_INSTALLED_DIR="1.8.0_51"
rpm -Uvh $JRE_RPM
alternatives --install "/usr/bin/java java /usr/java/jre$JRE_INSTALLED_DIR/bin/java 200000"
alternatives --install "/usr/bin/javaws javaws /usr/java/jre$JRE_INSTALLED_DIR/bin/javaws 200000"
#alternatives --install /usr/lib/mozilla/plugins/libjavaplugin.so libjavaplugin.so /usr/java/jre1.7.0_51/lib/i386/libnpjp2.so 200000
#alternatives --install /usr/lib64/mozilla/plugins/libjavaplugin.so libjavaplugin.so.x86_64 /usr/java/jre1.7.0_51/lib/amd64/libnpjp2.so 200000

#set the OS limit
LIMIT_FILE="/etc/security/limits.conf"
echo "" >> $LIMIT_FILE
echo "elasticsearch soft nofile 64000" >> $LIMIT_FILE
echo "elasticsearch hard nofile 64000" >> $LIMIT_FILE
#echo "elasticsearch soft nofile 64000" >> $LIMIT_FILE
#echo "elasticsearch hard nofile 64000" >> $LIMIT_FILE

ulimit -l unlimited

#install and run elasticsearch with default setting, kibana, marvel 
ES_VERSION="2.0.0"
ES_RPM="/vagrant/elasticsearch-$ES_VERSION.noarch.rpm"
if [ ! -f $ES_RPM ]; then
  wget -O $ES_RPM https://download.elasticsearch.org/elasticsearch/release/org/elasticsearch/distribution/rpm/elasticsearch/$ES_VERSION/elasticsearch-$ES_VERSION.rpm
fi
rpm -Uvh $ES_RPM

#/usr/share/elasticsearch/bin/plugin install mobz/elasticsearch-head
#/usr/share/elasticsearch/bin/plugin install karmi/elasticsearch-paramedic
#/usr/share/elasticsearch/bin/plugin install elasticsearch/marvel/latest 
/usr/share/elasticsearch/bin/plugin install license
/usr/share/elasticsearch/bin/plugin install marvel-agent

chsh -s /bin/bash elasticsearch
/sbin/chkconfig --add elasticsearch
