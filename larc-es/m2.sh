#!/bin/bash

chmod +x /vagrant/common.sh
/vagrant/common.sh

cp /etc/elasticsearch/elasticsearch.yml /etc/elasticsearch/elasticsearch.yml.bak
cp /vagrant/elasticsearch.2.m2.yml /etc/elasticsearch/elasticsearch.yml

#mv /var/lib/elasticsearch/coolclusterbro /var/lib/coolclusterbro_bak
#tar xzf /vagrant/m2-coolclusterbro.tar.gz -C /var/lib/elasticsearch
#chown -R elasticsearch:elasticsearch /var/lib/elasticsearch/coolclusterbro

chmod +x /vagrant/common-post.sh
/vagrant/common-post.sh
