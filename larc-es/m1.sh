#!/bin/bash

chmod +x /vagrant/common.sh
/vagrant/common.sh

cp /etc/elasticsearch/elasticsearch.yml /etc/elasticsearch/elasticsearch.yml.bak
cp /vagrant/elasticsearch.2.m1.yml /etc/elasticsearch/elasticsearch.yml

#tar xzf /vagrant/m1-coolclusterbro.tar.gz -C /var/lib/elasticsearch
#chown -R elasticsearch:elasticsearch /var/lib/elasticsearch/coolclusterbro

KIBANA_VERSION="4.2.0"
KIBANA_TAR_GZ="/vagrant/kibana-$KIBANA_VERSION-linux-x64.tar.gz"
if [ ! -f $KIBANA_TAR_GZ ]; then
  wget -O $ES_RPM https://download.elastic.co/kibana/kibana/kibana-$KIBANA_VERSION-linux-x64.tar.gz
fi
tar xzf $KIBANA_TAR_GZ -C /opt
ln -sf /opt/kibana-$KIBANA_VERSION-linux-x64 /opt/kibana
mv /opt/kibana/config/kibana.yml /opt/kibana/config/kibana.yml.bak
cp /vagrant/kibana.yml /opt/kibana/config/kibana.yml
# /opt/kibana/bin/kibana&

chmod +x /vagrant/common-post.sh
/vagrant/common-post.sh

#install and run nginx with default setting
#cp /vagrant/nginx.repo /etc/yum.repos.d/nginx.repo
#yum search nginx
#yum install -y nginx
#/sbin/chkconfig --add nginx
#service nginx start
# 
##install kibana in home directory
#KIBANA_VERSION="3.0.1"
#wget https://download.elasticsearch.org/kibana/kibana/kibana-$KIBANA_VERSION.tar.gz
#tar -xzf kibana-$KIBANA_VERSION.tar.gz
