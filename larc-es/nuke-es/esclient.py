from elasticsearch import Elasticsearch, helpers
import elasticsearch
from datetime import datetime

DATETIME_FORMAT = '%Y%m%d %H:%M:%S'

class ESClient(object):
    def __init__(self,hosts,batchSize=200):
        self.es = Elasticsearch(hosts, timeout=600)
        self.actions = []
        self.batchSize = batchSize

    def _checkBulk(self, force=False):
        l = len(self.actions)
        if l >= self.batchSize or force:
            stat = helpers.bulk(self.es, self.actions, False, chunk_size=l)
            del self.actions[:l]
            print "{1} - Performed {0} actions".format(l,datetime.now().strftime(DATETIME_FORMAT))
            print stat

    # Indexing documents
    def indexDocument(self, indexName, typeName, id, data):
        self.es.index(index=indexName, doc_type=typeName, id=id, data=data)

    def indexDocumentInBulk(self, indexName, typeName, id, data):
        action = { "_index": indexName, "_type": typeName, "_id": id, "_source": data }
        self.actions.append(action)
        self._checkBulk()

    def deleteDocumentInBulk(self, indexName, typeName, id):
        action = { "_op_type": "delete", "_index": indexName, "_type": typeName, "_id": id }
        self.actions.append(action)
        self._checkBulk()

    def updateDocumentInBulk(self, indexName, typeName, id, data):
        action = { "_op_type": "update", "_index": indexName, "_type": typeName, "_id": id , "doc": data}
        self.actions.append(action)
        self._checkBulk()

    def getDocument(self, indexName, typeName, id):
        try:
            rec = self.es.get(index=indexName, doc_type=typeName, id=id)
            if '_source' in rec:
                res = rec['_source']
                res['_id'] = rec['_id']
                return res
        except elasticsearch.exceptions.NotFoundError as e:
            print e
        return None

    def searchAndCount(self, indexName, typeName, query):
        rec = self.es.search(index=indexName, doc_type=typeName, body=query, search_type='count')
        if 'hits' in rec and 'total' in rec['hits']:
            return rec['hits']['total']
        return 0

    # Scan & Scroll
    def scanAndScroll(self, indexName, typeName, scroll='5m', size=250):
        for rec in helpers.scan(self.es, index=indexName, doc_type=typeName, size=size, scroll=scroll):
            res = rec['_source']
            res['_id'] = rec['_id']
            yield res

    # Search & Scroll
    def searchAndScroll(self, indexName, typeName, query, size=250, scroll='5m'):
        for rec in helpers.scan(self.es, query=query, index=indexName, doc_type=typeName, size=size, scroll=scroll):
            res = rec['_source']
            res['_id'] = rec['_id']
            yield res

    # Public: Finishing
    def cleanup(self):
        self._checkBulk(force=True)

    def finish(self):
        self._checkBulk(force=True)


