from esclient import ESClient

hostAddr = ['192.168.1.11','192.168.1.12','192.168.1.14']
portNumber = 9200
esClient = ESClient([{'host':h,'port':portNumber} for h in hostAddr])

def indexOneDocument(seed):
    esClient.indexDocumentInBulk(indexName='test_index',typeName='test', id=str(seed), data={str(seed):seed})

def indexInfiniteDocuments():
    seed = 270000
    while True:
        indexOneDocument(seed)
        seed += 1

indexInfiniteDocuments()

