#!/bin/bash

PROVISIONED="/var/vagrant_provision"
if [ -f $PROVISIONED ]; then
   echo "VM is already provisioned!" 
   exit 0
fi

apt-get update
apt-get install -y apache2
rm -rf /var/www
ln -fs /vagrant /var/www

apt-get install -y vim

touch $PROVISIONED
