#!/bin/bash

EPEL_RPM=/vagrant/epel-release-6-8.noarch.rpm
if [ ! -e EPEL_RPM ];then
  wget http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm -O $EPEL_RPM
fi
rpm -Uvh $EPEL_RPM
yum install -y man vim git mlocate zip htop iotop lsof tcpdump sysstat
#to build Julia
#yum install -y gcc gcc-c++ gcc-gfortran perl wget curl patch m4 
updatedb
