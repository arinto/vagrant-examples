#!/bin/bash

#install additional utilities here
yum install -y unzip

if [ ! -f bin/jq ];then 
  wget http://stedolan.github.io/jq/download/linux64/jq -P bin
  chmod +x bin/jq
  chown vagrant:vagrant bin/jq
fi

#install Oracle JRE
JRE_RPM="/vagrant/jre-7u17-linux-x64.rpm"
if [ ! -f $JRE_RPM ]; then
  wget --no-cookies --no-check-certificate --header "Cookie: oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/7u17-b02/jre-7u17-linux-x64.rpm" -O $JRE_RPM 
fi

rpm -Uvh $JRE_RPM
alternatives --install /usr/bin/java java /usr/java/jre1.7.0_17/bin/java 200000
alternatives --install /usr/bin/javaws javaws /usr/java/jre1.7.0_17/bin/javaws 200000
#alternatives --install /usr/lib/mozilla/plugins/libjavaplugin.so libjavaplugin.so /usr/java/jre1.7.0_51/lib/i386/libnpjp2.so 200000
#alternatives --install /usr/lib64/mozilla/plugins/libjavaplugin.so libjavaplugin.so.x86_64 /usr/java/jre1.7.0_51/lib/amd64/libnpjp2.so 200000

PLAY_DIST=insta-geo-ws-1.0-SNAPSHOT
cp /vagrant/$PLAY_DIST.zip .
unzip $PLAY_DIST.zip
chmod +x $PLAY_DIST/bin/insta-geo-ws
