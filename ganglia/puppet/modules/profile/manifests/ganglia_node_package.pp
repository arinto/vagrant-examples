class profile::ganglia_node_package{
  package{'ganglia-gmond':
    ensure => present,
    allow_virtual => true,
  } 
}
