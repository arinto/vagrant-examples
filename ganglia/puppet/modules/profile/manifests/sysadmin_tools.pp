class profile::sysadmin_tools{
  package{'vim':
    ensure => present,
    allow_virtual => true,
  }  
}
