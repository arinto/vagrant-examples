class profile::ganglia_master_package{
  package{'ganglia-web':
    ensure => present,
    allow_virtual => true,
  }
}
