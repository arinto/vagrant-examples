#!/bin/bash

#installing Python 2.7 dependencies and libraries
yum groupinstall -y "Development tools"
yum install -y zlib-devel bzip2-devel openssl-devel ncurses-devel sqlite-devel readline-devel tk-devel gdbm-devel db4-devel libpcap-devel xz-devel

wget --no-check-certificate http://python.org/ftp/python/2.7.6/Python-2.7.6.tar.xz
tar xf Python-2.7.6.tar.xz
cd Python-2.7.6
./configure --prefix=/usr/local --enable-unicode=ucs4 --enable-shared LDFLAGS="-Wl,-rpath /usr/local/lib"
make && make altinstall

wget https://bitbucket.org/pypa/setuptools/raw/bootstrap/ez_setup.py
PYTHON_INSTALL_DIR="/usr/local/bin"
$PYTHON_INSTALL_DIR/python2.7 ez_setup.py
$PYTHON_INSTALL_DIR/easy_install-2.7 pip
$PYTHON_INSTALL_DIR/pip2.7 install virtualenv

#installing scrappy's dependencies
yum install -y python2-devel libffi-devel openssl-devel libxslt-devel 
#$PYTHON_INSTALL_DIR/pip2.7 install scrapy

chown -R vagrant:vagrant /usr/local
