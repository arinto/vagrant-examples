#!/bin/bash

#install additional utilities here
yum install -y vim mlocate
 
#install Oracle JRE
JRE_RPM="jre-7u51-linux-x64.rpm"
wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/7u51-b13/jre-7u51-linux-x64.rpm" -O $JRE_RPM
rpm -Uvh $JRE_RPM
alternatives --install /usr/bin/java java /usr/java/jre1.7.0_51/bin/java 200000
alternatives --install /usr/bin/javaws javaws /usr/java/jre1.7.0_51/bin/javaws 200000
alternatives --install /usr/lib/mozilla/plugins/libjavaplugin.so libjavaplugin.so /usr/java/jre1.7.0_51/lib/i386/libnpjp2.so 200000
alternatives --install /usr/lib64/mozilla/plugins/libjavaplugin.so libjavaplugin.so.x86_64 /usr/java/jre1.7.0_51/lib/amd64/libnpjp2.so 200000

#install and run elasticsearch with default setting
ES_VERSION=1.1.0
wget https://download.elasticsearch.org/elasticsearch/elasticsearch/elasticsearch-$ES_VERSION.noarch.rpm
rpm -ivh elasticsearch-$ES_VERSION.noarch.rpm
/sbin/chkconfig --add elasticsearch
service elasticsearch start

/usr/share/elasticsearch/bin/plugin --install mobz/elasticsearch-head
/usr/share/elasticsearch/bin/plugin --install karmi/elasticsearch-paramedic

#install logstash
LOGSTASH_VERSION=1.4.0-1_c82dc09
wget https://download.elasticsearch.org/logstash/logstash/packages/centos/logstash-$LOGSTASH_VERSION.noarch.rpm
rpm -ivh logstash-$LOGSTASH_VERSION.noarch.rpm

#install and run nginx with default setting
cp /vagrant/nginx.repo /etc/yum.repos.d/nginx.repo
yum search nginx
yum install -y nginx
/sbin/chkconfig --add nginx
service nginx start

#install kibana in home directory
KIBANA_VERSION=3.0.1
wget https://download.elasticsearch.org/kibana/kibana/kibana-$KIBANA_VERSION.tar.gz
tar -xzf kibana-$KIBANA_VERSION.tar.gz
